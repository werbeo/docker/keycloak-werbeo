FROM jboss/keycloak:4.3.0.Final
MAINTAINER dve@vergien.net

ARG KEYCLOAK_ADMIN_USER=admin
ARG KEYCLOAK_ADMIN_PASWORD=password
ENV DB_VENDOR=h2

RUN /opt/jboss/keycloak/bin/add-user.sh -u ${KEYCLOAK_ADMIN_USER} -p ${KEYCLOAK_ADMIN_PASWORD}

ADD import-realm.json /opt/jboss/keycloak/
ADD keycloak-add-user.json /opt/jboss/keycloak/standalone/configuration/

ENTRYPOINT [ "/opt/jboss/docker-entrypoint.sh" ]

CMD ["-b", "0.0.0.0", "-Dkeycloak.import=/opt/jboss/keycloak/import-realm.json"]

